﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class SolarSystemGenerator
    {
        public SolarSystemGenerator() { }

        public SolarSystem Generate()
        {
            RandomSingleton random = RandomSingleton.Instance();
            SolarSystem solarSystem = new SolarSystem();
        
            var tasks = new List<Task>();
            
            
            tasks.Add(Task.Factory.StartNew(() =>
                {
                    solarSystem.Star = new StarGenerator().Generate();
                }));

            tasks.Add(Task.Factory.StartNew(() =>
                {
                    PlanetGenerator planetGenerator = new PlanetGenerator();
                    int planetCount = random.Next((int)Math.Sqrt((double)solarSystem.Star.Class), (int)Math.Sqrt(Math.Pow((double)solarSystem.Star.Class, 3.5)));
                    for (int i = 0; i < planetCount; i++)
                    {
                        solarSystem.Planets.Add(planetGenerator.Generate());

                    }
                    //}
                    //Parallel.For(0, planetCount, (i) =>
                    //{
                    //    solarSystem.Planets.Add(planetGenerator.Generate());
                    //});
                }));

           tasks.Add(Task.Factory.StartNew(() =>
                {
                    CometGenerator cometGenerator = new CometGenerator();
                    int cometCount = random.Next(250);
                    for (int i = 0; i < cometCount; i++)
                    {
                        solarSystem.Comets.Add(cometGenerator.Generate());
                    }

                    //Parallel.For(0, cometCount, (i) =>
                    //{
                    //    solarSystem.Comets.Add(cometGenerator.Generate());
                    //});
                }));

            tasks.Add(Task.Factory.StartNew(() =>
                {
                    double asteroidBeltChance = random.NextDouble();
                    if (asteroidBeltChance < 0.2)
                        solarSystem.AsteroidBelt = new AsteroidBeltGenerator().Generate();
                }));


            //Freezes UI
            //Task.WaitAll(starTask, planetTask, cometTask, asteroidTask);


            //Task.Factory.ContinueWhenAll(tasks);
            return solarSystem;
        }
    }
}
