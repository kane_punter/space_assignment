﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class StarGenerator
    {
        public StarGenerator() { }

        public Star Generate()
        {
            Star star = new Star();

            RandomSingleton random = RandomSingleton.Instance();

            star.Class = (StarClass)random.Next((int)StarClass.Count);

            switch (star.Class)
            {
                case StarClass.O:
                    star.Temperature = random.Next(30000, 1000000);
                    star.ColourDescription = "Blue";
                    star.Mass = random.NextDouble() * (20.0 - 16.0) + 16.0;
                    star.Radius = random.NextDouble() * (10.0 - 6.6) + 6.6;
                    break;
                case StarClass.B:
                    star.Temperature = random.Next(10000, 30000);
                    star.ColourDescription = "Blue White";
                    star.Mass = random.NextDouble() * (16.0 - 2.1) + 2.1;
                    star.Radius = random.NextDouble() * (6.6 - 1.8) + 6.6;
                    break;
                case StarClass.A:
                    star.Temperature = random.Next(7500, 10000);
                    star.ColourDescription = "White";
                    star.Mass = random.NextDouble() * (2.1 - 1.4) + 1.4;
                    star.Radius = random.NextDouble() * (1.8 - 1.4) + 6.6;
                    break;
                case StarClass.F:
                    star.Temperature = random.Next(6000, 7500);
                    star.ColourDescription = "Yellow White";
                    star.Mass = random.NextDouble() * (1.4 - 1.04) + 1.04;
                    star.Radius = random.NextDouble() * (1.4 - 1.15) + 6.6;
                    break;
                case StarClass.G:
                    star.Temperature = random.Next(5200, 6000);
                    star.ColourDescription = "Yellow";
                    star.Mass = random.NextDouble() * (1.04 - 0.8) + 0.8;
                    star.Radius = random.NextDouble() * (1.15 - 0.96) + 6.6;
                    break;
                case StarClass.K:
                    star.Temperature = random.Next(3700, 5200);
                    star.ColourDescription = "Orange";
                    star.Mass = random.NextDouble() * (0.8 - 0.45) + 0.45;
                    star.Radius = random.NextDouble() * (0.96 - 0.7) + 6.6;
                    break;
                case StarClass.M:
                    star.Temperature = random.Next(2400, 3700);
                    star.ColourDescription = "Red";
                    star.Mass = random.NextDouble() * (0.45 - 0.08) + 0.08;
                    star.Radius = random.NextDouble() * 0.7;
                    break;
            }

            return star;
        }
    }
}
